from conans import ConanFile, CMake, tools
from conans.tools import download, unzip, os_info
import os

class NanaConan(ConanFile):
    name = "Nana"
    version = "1.6.2"
    license = "MIT"
    url = "https://github.com/jacmoe/conan-nana"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "png": [True, False], "jpeg": [True, False]}
    default_options = "shared=False", "png=False", "jpeg=False"
    generators = "cmake"

    def system_requirements(self):

        if self.settings.os == "Linux":
            self.run('sudo apt-get install -y xorg-dev libboost-system-dev libboost-filesystem-dev libboost-thread-dev libstdc++-4.8-dev libasound2-dev')

    def source(self):
        self.run("git clone https://github.com/cnjinhao/nana.git")
        self.run("cd nana && git checkout -t origin/hotfix-1.6.2")
        tools.replace_in_file("nana/CMakeLists.txt", "project(nana)", '''project(nana)
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()''')

    def build(self):
        cmake = CMake(self)
        shared = "-DBUILD_SHARED_LIBS=ON" if self.options.shared else ""
        png = "-DNANA_CMAKE_ENABLE_PNG:BOOL=ON" if self.options.png else ""
        jpeg = "-DNANA_CMAKE_ENABLE_JPEG:BOOL=ON" if self.options.jpeg else ""
        self.run('cmake nana %s %s %s %s' % (cmake.command_line, shared, png, jpeg))
        self.run("cmake --build . %s" % cmake.build_config)

    def package(self):
        self.copy("*", dst="include", src="nana/include")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.cppflags = ["-std=c++11"]
        if self.settings.build_type == "Debug":
            self.cpp_info.libs = ["nana_d"]
        else:
            self.cpp_info.libs = ["nana"]
        if os_info.is_linux:
            self.cpp_info.libs.append("pthread")
            self.cpp_info.libs.append("X11")
            self.cpp_info.libs.append("boost_system")
            self.cpp_info.libs.append("boost_filesystem")
            self.cpp_info.libs.append("boost_thread")
            self.cpp_info.libs.append("Xft")
            self.cpp_info.libs.append("fontconfig")
            self.cpp_info.libs.append("stdc++fs")
        if os_info.is_linux and self.options.png:
            self.cpp_info.libs.append("png")
        if os_info.is_linux and self.options.jpeg:
            self.cpp_info.libs.append("jpeg")
        
