#pragma once

#include <boost/algorithm/string.hpp>
#include <fmt/format.h>

#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include <sstream>
#include <numeric>
#include <sstream>


struct Coordinates {
    std::string lat;
    std::string lon;
    std::string hh;
    std::string mm;
    std::string ss; 
};

auto operator<<(std::ostream& os, const Coordinates& coor) -> std::ostream&;

class nmea
{
private:
    std::string                 last_valid_str_;
    std::string                 buffer_;
    std::vector<std::string>    last_valid_trame_;
    Coordinates                 coordinates_;
    bool                        has_valid_data_ = false;
    

    auto process(std::size_t dollar_pos, std::size_t return_pos) -> void;

public:
    nmea()  = default; 
    ~nmea() = default;

    auto checksum(std::string const& str)   const           -> bool;
    auto append(std::string data)                           -> void;
    auto has_valid_data()                   const noexcept  -> bool;
    auto coordinates()                      const noexcept  -> Coordinates;
    auto string()                           const noexcept  -> std::string;
    
};