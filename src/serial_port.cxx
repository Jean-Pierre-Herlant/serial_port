#include "serial_port.hpp"
 
serial_port::serial_port(std::string const& com, int baudrate)
{
    port_ = ::open(com.c_str(), O_RDWR | O_NOCTTY);
 
    if(port_ < 0){
        perror("open: ");
        return;
    }
 
    is_open_ = true;
 
    buffer_.resize(50);
 
    tcgetattr(port_, &tty_);
    cfsetospeed(&tty_, baudrate);
    cfsetispeed(&tty_, baudrate);
    tty_.c_iflag = 0;
    tty_.c_lflag = 0;
    tty_.c_oflag = 0;
    tty_.c_cc[VMIN] = 1;
    tty_.c_cc[VTIME] = 0;
    tty_.c_cflag &= ~CRTSCTS;
    tty_.c_iflag &= ~((IXON - IXOFF) | IXANY);
    tty_.c_cflag &= ~(PARENB | CSTOPB | CSIZE);
    tty_.c_cflag |= CS8;
    tty_.c_cflag |= (CLOCAL | CREAD);
    tcflush(port_, TCSANOW);
    tcsetattr(port_, TCSANOW, &tty_);
}
 
auto serial_port::is_open() const noexcept -> bool
{
    return is_open_;
}
 
auto serial_port::read() -> std::string 
{
    int size = ::read(port_, buffer_.data(), buffer_.size());
 
    return std::string(buffer_.begin(), buffer_.begin() + size);
}

auto serial_port::write(std::vector<char> vec) const -> int
{
    return vec.size(); 
}

