#include <fmt/format.h>
#include <iostream>
#include <string>
 
#include "serial_port.hpp"
#include "nmea.hpp"

auto main() -> int
{
    auto serial = serial_port("/dev/ttyUSB0", 9600);
 
    if(!serial.is_open())
	{
        std::cerr << "no serial port connected" << std::endl;
        return 1;
    }

    auto trame_nmea = nmea();
 
    while(true)
    {
		trame_nmea.append(serial.read());

        if(trame_nmea.has_valid_data())
        {
            std::cout << trame_nmea.coordinates(); 
        }   
    }  
}




