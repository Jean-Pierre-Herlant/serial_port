#include "nmea.hpp"

auto nmea::append(std::string data) -> void
{
    //$GPGLL,4952.459,N,00218.095,E,130927,A*21*

    buffer_.append(data);
    
    auto dollar_pos = buffer_.find_first_of('$');

    if(dollar_pos != std::string::npos)
    {
        auto return_pos = buffer_.find_first_of('\n', dollar_pos);

        if(return_pos != std::string::npos)
        {
            this->process(dollar_pos, return_pos);
            buffer_ = std::string(buffer_.begin() + return_pos, buffer_.end()); 
        }  
    }
}

auto nmea::process(std::size_t dollar_pos, std::size_t return_pos) -> void
{

    auto tmp = buffer_.substr(dollar_pos, return_pos);

    if(    tmp.size() < 40 
        or std::count(tmp.begin(), tmp.end(), ',') != 6 
        or !this->checksum(tmp))
    {
        return;
    }

    last_valid_str_ = tmp;
    
    boost::split(last_valid_trame_, last_valid_str_, boost::is_any_of(","));

    if(     last_valid_trame_.at(1).size() == 8 
        and last_valid_trame_.at(3).size() == 9 
        and last_valid_trame_.at(5).size() == 6)
    {
        coordinates_.lat = last_valid_trame_.at(1);
        coordinates_.lon = last_valid_trame_.at(3);
        coordinates_.hh  = last_valid_trame_.at(5).substr(0, 2);
        coordinates_.mm  = last_valid_trame_.at(5).substr(2, 2);
        coordinates_.ss  = last_valid_trame_.at(5).substr(4, 2);

        has_valid_data_ = true;
    }
    
}

auto nmea::coordinates() const noexcept -> Coordinates
{
    return coordinates_;
}

auto nmea::string() const noexcept  -> std::string
{
    return (last_valid_str_.size() > 0) ? last_valid_str_ : "No data received yet. Call has_valid_data first\n";
}

auto nmea::has_valid_data() const noexcept -> bool
{
    return has_valid_data_;
}

auto nmea::checksum(std::string const& str) const -> bool
{
    auto star_pos = str.find("*");

    if(star_pos == std::string::npos)
    {
        return false;
    }

    auto received_chk = str.substr(star_pos);

    if(received_chk.size() != 5)
    {
        return false; 
    }

    received_chk = received_chk.substr(1, 2);

    char checksum = std::accumulate(str.begin()+1, str.end() - 5, 0,
        [](char sum, char ch) { return sum ^ ch; });

    return received_chk == fmt::format("{:X}", checksum);
    
}

auto operator<<(std::ostream& os, const Coordinates& coor) -> std::ostream&
{
    return os   << fmt::format("[{0}:{1}:{2}] ", coor.hh, coor.mm, coor.ss)
                << fmt::format("lat: {0} lon: {1}", coor.lat, coor.lon)
                << std::endl;   
}