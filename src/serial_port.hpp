#pragma once
 
#include <unistd.h>
#include <termios.h>
#include <fcntl.h>
 
#include <iostream>
#include <vector>
 
class serial_port 
{
private:
    std::vector<char>   buffer_;
    termios             tty_;
    bool                is_open_ = false;
    int                 port_;
   
public:
    explicit serial_port(std::string const&, int baudrate);
    serial_port()  = delete;
    ~serial_port() = default;
 
    auto write(std::vector<char> vec)   const           -> int;
    auto is_open()                      const noexcept  -> bool;
    auto read()                                         -> std::string;
};